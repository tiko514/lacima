﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lacima.OutputWriters
{
    class ConsoleOutputWriter : IOutputWriter
    {
        public void WriteOutput(QuotesDictionary quotes)
        {
            List<string> shortHands = quotes.ShortHands;

            Console.Write($"{DateTime.Now}");

            foreach (var shortHand in shortHands)
            {
                Console.Write($"\t{shortHand}");
            }

            Console.WriteLine();

            foreach (var date in quotes.GetDictionary())
            {
                Console.Write($"{date.Key}");

                foreach (var shortHand in shortHands)
                {
                    var output = quotes.ContainsKey(date.Key, shortHand) ? quotes[date.Key, shortHand] : null;
                    Console.Write($"\t{output}");
                }

                Console.WriteLine();
            }
        }
    }
}
