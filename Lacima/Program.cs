﻿using Lacima.OutputWriters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Lacima
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Please drag and drop csv file here and click <Enter>: ");
            string filePath = Console.ReadLine().Replace(@"""", "");

            List<Quote> inputQuotes = new List<Quote>();

            try
            {
                using (var reader = new StreamReader(filePath))
                {
                    reader.ReadLine();      // First row for column names

                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(',');

                        Quote inputQuote = new Quote
                        {
                            ObservationDate = values[0],
                            ShortHand = values[1],
                            FromDate = values[2],
                            ToDate = values[3],
                            Price = values[4]
                        };

                        // shortHand like regex Q[1-4][_][0-9][0-9]
                        if (!Regex.IsMatch(inputQuote.ShortHand, "Q[1-4][_][0-9][0-9]") && inputQuote.QuarterDatesAreValid)
                        {
                            inputQuote.ShortHand = Quote.ComposeShortHandFromDates(Convert.ToDateTime(inputQuote.FromDate));
                        }

                        float price;
                        bool priceIsValid = float.TryParse(inputQuote.Price, out price);

                        // Duplicates
                        // Shorthand is invalid only if it doesn't match startdate and enddate, and the shorthand with correct startdate and enddate exists
                        if (inputQuote.ObservationDateIsValid && priceIsValid)
                        {
                            var existingQuote = inputQuotes.FirstOrDefault(q => q.ObservationDate == inputQuote.ObservationDate && q.ShortHand == inputQuote.ShortHand);
                            if (existingQuote != null)
                            {
                                if (float.Parse(existingQuote.Price, CultureInfo.InvariantCulture.NumberFormat) != 0)
                                {
                                    Console.WriteLine($"observationDate: {inputQuote.ObservationDate}, shortHand: {inputQuote.ShortHand} - {price}");
                                    inputQuote.AdjustWith(inputQuotes);
                                }
                            }
                            else
                            {
                                inputQuotes.Add(inputQuote);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            QuotesDictionary quotesDict = new QuotesDictionary();
            foreach(var inputQuote in inputQuotes)
            {
                DateTime observationDate = Convert.ToDateTime(inputQuote.ObservationDate);
                quotesDict[observationDate, inputQuote.ShortHand] = float.Parse(inputQuote.Price, CultureInfo.InvariantCulture.NumberFormat);
            }


            IOutputWriter outputWriter = new CsvOutputWriter(filePath.Replace(".csv", " out.csv"));
            // IOutputWriter outputWriter = new DataTableOutputWriter();
            outputWriter.WriteOutput(quotesDict);

            // Console.ReadLine();
        }
    }
}
