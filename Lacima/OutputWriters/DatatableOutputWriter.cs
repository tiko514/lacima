﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Lacima.OutputWriters
{
    public class DataTableOutputWriter : IOutputWriter
    {
        public DataTable output { get; private set; }

        public void WriteOutput(QuotesDictionary quotes)
        {
            List<string> shortHands = quotes.ShortHands;

            output = new DataTable();
            output.Columns.Add(new DataColumn("ObservationDate"));
            output.Columns.AddRange(shortHands.Select(sh => new DataColumn(sh)).ToArray());

            foreach (var obsDateItem in quotes.GetDictionary())
            {
                DataRow dr = output.NewRow();
                dr["ObservationDate"] = obsDateItem.Key;

                foreach (var item in obsDateItem.Value)
                {
                    dr[item.Key] = item.Value;
                }

                output.Rows.Add(dr);
            }
        }
    }
}
