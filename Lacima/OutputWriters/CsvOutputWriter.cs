﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Lacima.OutputWriters
{
    class CsvOutputWriter : IOutputWriter
    {
        string _outputFilePath;

        public CsvOutputWriter(string outputFilePath)
        {
            _outputFilePath = outputFilePath;
        }

        public void WriteOutput(QuotesDictionary quotes)
        {
            List<string> shortHands = quotes.ShortHands;

            StringBuilder csv = new StringBuilder(",");

            csv.AppendLine(string.Join(',', shortHands));

            foreach (var date in quotes.GetDictionary())
            {
                string newLine = $"{date.Key},";

                foreach (var shortHand in shortHands)
                {
                    float? output = quotes.ContainsKey(date.Key, shortHand) ? quotes[date.Key, shortHand] : null;
                    newLine += $"{output.ToString()},";
                }

                csv.AppendLine(newLine);
            }

            File.WriteAllText(_outputFilePath, csv.ToString());
        }
    }
}
