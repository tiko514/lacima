﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lacima
{
    public class QuotesDictionary
    {
        private readonly Dictionary<DateTime, Dictionary<string, float?>> _dictionary = new Dictionary<DateTime, Dictionary<string, float?>>();

        public float? this[DateTime observationDate, string shortHand]
        {
            get
            {
                return _dictionary[observationDate][shortHand];
            }

            set
            {
                if (!_dictionary.ContainsKey(observationDate))
                {
                    _dictionary[observationDate] = new Dictionary<string, float?>();
                }

                _dictionary[observationDate][shortHand] = value;
            }
        }
        
        public bool ContainsKey(DateTime date, string shortHand)
        {
            if (_dictionary.ContainsKey(date))
            {
                return _dictionary[date].ContainsKey(shortHand);
            }

            return false;
        }
        
        public Dictionary<DateTime, Dictionary<string, float?>> GetDictionary()
        {
            return _dictionary;
        }

        public List<string> ShortHands
        {
            get
            {
                List<string> shortHands = new List<string>();

                foreach (var dictValue in _dictionary.Values)
                {
                    shortHands.AddRange(dictValue.Keys);
                }

                shortHands.Remove("");
                shortHands = shortHands.Distinct().OrderBy(sh => sh.Length > 2 ? sh.Substring(sh.Length - 2) : sh).ThenBy(sh => sh[1]).ToList();

                return shortHands;
            }
        }
    }
}
