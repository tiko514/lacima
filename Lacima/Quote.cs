﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lacima
{
    class Quote
    {
        public string ObservationDate { get; set; }

        public string ShortHand { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public string Price { get; set; }


        public bool shortHandMatchesDates
        {
            get
            {
                return QuarterDatesAreValid && ShortHand == ComposeShortHandFromDates(Convert.ToDateTime(FromDate));
                /* the case is already covered in QuarterDatesAreValid property
                 * && ShortHand == ComposeShortHandFromDates(Convert.ToDateTime(ToDate))*/
            }
        }


        public static string ComposeShortHandFromDates(DateTime date)
        {
            int quarter = (date.Month - 1) / 3 + 1;
            string year = date.ToString("yy");

            return $"Q{quarter}_{year}";
        }

        public static string ComposeShortHandFromDates(string date)
        {
            return ComposeShortHandFromDates(Convert.ToDateTime(date));
        }


        public bool QuarterDatesAreValid
        {
            get
            {
                DateTime _fromDate, _toDate;
                if (!DateTime.TryParse(FromDate, out _fromDate))
                    return false;

                if (!DateTime.TryParse(ToDate, out _toDate))
                    return false;

                return !(_fromDate.Month % 3 != 1
                    || _fromDate.Day != 1
                    || _toDate.Month % 3 != 0
                    || _toDate.Day != DateTime.DaysInMonth(_toDate.Year, _toDate.Month)
                    || _fromDate.Year != _toDate.Year
                    || (_fromDate.Month - 1) / 3 != (_toDate.Month - 1) / 3);
            }
        }


        public void AdjustWith(List<Quote> quotesToAdjustWith)
        {
            Quote conflictingQuote = quotesToAdjustWith.First(oq => oq.ShortHand == ShortHand);
            if (conflictingQuote.shortHandMatchesDates && !this.shortHandMatchesDates)
            {
                ShortHand = ComposeShortHandFromDates(FromDate);
                // For the case if after the update there's still a conflicting quote with existing quotes
                AdjustWith(quotesToAdjustWith); // but in this case out current(this) quote will be valid
            }
            else if (!conflictingQuote.shortHandMatchesDates && this.shortHandMatchesDates)
            {
                quotesToAdjustWith.Remove(conflictingQuote);
                quotesToAdjustWith.Add(this);

                conflictingQuote.AdjustWith(quotesToAdjustWith);
            }

            quotesToAdjustWith.Add(this);
        }

        public override String ToString()
        {
            return $"ObservationDate: {ObservationDate}, ShortHand: {ShortHand}, FromDate: {FromDate}, ToDate: {ToDate}, Price: {Price}";
        }

        public bool ObservationDateIsValid
        {
            get
            {
                DateTime fromDate, observationDate;
                if (DateTime.TryParse(FromDate, out fromDate) && DateTime.TryParse(ObservationDate, out observationDate))
                {
                    if (observationDate < fromDate)
                    {
                        return true;
                    }
                    else
                    {
                        Console.WriteLine($"Wrong quote: {ToString()}");
                    }
                }

                return false;
            }
        }
    }
}