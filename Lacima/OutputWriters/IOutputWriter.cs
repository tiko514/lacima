﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lacima.OutputWriters
{
    public interface IOutputWriter
    {
        void WriteOutput(QuotesDictionary quotes);
    }
}
